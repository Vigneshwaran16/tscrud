import { Response, Request } from 'express'
import { MovieDbModel, modelSchema } from '../model/model'
import {Service} from '../service/services'

// Controller handles the request and response

let service : Service
//let movieEntry: MovieDbModel
export class Controller {
    constructor(){
        service = new Service()
    }

    // All the errors thrown by db are caught implicitly in catch block
    // Other errors are to be handled manually using throw and catch
    async getEntry(req : Request , res : Response){   
        try{
            const entries: MovieDbModel[] = await service.getEntry()
            if(entries == null || entries == []){
                throw "Empty Database"
            }
            else{
                res.status(200).json({movies: entries})
            }
        }
        catch(error){
            res.status(400).json({error: error})
        }
    }
    

    async createEntry(req : Request, res: Response){
        try{
            const movieEntry : MovieDbModel = await service.createEntry(req.body)
            if(movieEntry == null  || movieEntry == undefined){
                throw "Cannot create Entry"
            }
            else{
                res.status(200).json({created_entry: movieEntry})
            }
         }
         catch(error){
            if(error.name == "ValidationError"){
                res.status(400).json({error: "Please add fields as per Schema"})
            }
            else{
                res.status(500).json({error : error})
            }
         }

    }

    
    public async deleteEntry(req: Request, res: Response){
        try{
            const movieEntry: MovieDbModel = await service.deleteEntry(req)
            if(movieEntry == null || movieEntry == undefined){
                throw "Cannot delete Entry"
            }
            else{
                res.status(200).json({deleted: movieEntry})
            }
        }
        catch(error){
            res.status(400).json({error: error})
        }

    }

    
    public async getById(req: Request, res: Response){
        try{
            const movieEntry: MovieDbModel = await service.getById(req)
            if(movieEntry == null || movieEntry == undefined){
                throw "Cannot find the movie with this id"
            }
            else{
                res.status(200).json({movie: movieEntry})
            }
        }
        catch(error){
            res.status(400).json({error: error})
        }
        
    }

    
    public async updateEntry(req: Request, res: Response){
        try{
            const movieEntry: MovieDbModel = await service.updateEntry(req)
            if(movieEntry == null || movieEntry == undefined){
                throw "Cannot update Entry"
            }
            else{
                res.status(200).json({updated: movieEntry})
            }
        }
        catch(error){
            res.status(400).json({error: error})
        }
        
    }
}