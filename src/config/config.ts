import mongoose  from 'mongoose'

export class MongoSetup{
    public mongoUrl = 'mongodb://localhost:27017/tsDB'

    public connectDB(){
        mongoose.connect(this.mongoUrl,{ useUnifiedTopology: true , useNewUrlParser: true })
    }
}