import * as mongoose from 'mongoose'

const Schema = mongoose.Schema


export interface MovieDbModel extends mongoose.Document{
    _id : String,
    movieTitle : String,
    director : String,
    cast : {
        actor : String,
        actress : String,
    },
    __v : number
}

export const modelSchema  = new Schema({
    movieTitle: {
        type: String,
        required : true
    },
    director: {
        type: String,
        required : true
    },
    cast :{
        type:{ 
            actor: String,
            actress: String
        },
        required: true
    }
}
)


//export const Model = mongoose.model('tsDB', modelSchema, 'movies')
