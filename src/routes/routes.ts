import { Request, Response , Application, Router} from 'express'
import {Controller} from '../controller/controller'

//const controller = new Controller();

export class Routes{
    private controller: Controller = new Controller();

    public routes(app: Application): void {
        app.get('/get',this.controller.getEntry)
        app.post('/create',this.controller.createEntry)
        app.delete('/remove/:id',this.controller.deleteEntry)
        app.get('/get/:id',this.controller.getById)
        app.put('/update/:id',this.controller.updateEntry)
    }
}