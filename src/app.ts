import express, {Application, Request, Response} from 'express'
import {Routes} from './routes/routes'
import {MongoSetup} from './config/config'
import mongoose from 'mongoose'

const serverPort = 7070

class App{

    public app: Application
    public routes: Routes
    constructor(){
        this.app = express()
        this.routes = new Routes()
        this.intialize()
        this.routes.routes(this.app)
    }

    private intialize(){
        this.app.use(express.json())
        this.mongosetup()
    }

    private mongosetup(){
        let mongoconnect: MongoSetup = new MongoSetup()
        mongoconnect.connectDB()
        mongoose.connection.on('connected', () => {
            console.log('DB Connected')
        })
    }
}

const appObj = new App()

appObj.app.listen(serverPort, () => {
    console.log("Server running on port ", serverPort)
})


