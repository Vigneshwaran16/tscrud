import { Request } from 'express'
//import {Model} from '../model/model'
import { Dao } from '../dao/dao'
import { MovieDbModel } from '../model/model';

// Services handle the business logic of the application
// Here, necessary steps for the operations and error handling are done here

let dao: Dao
export class Service{
    
    constructor(){
        dao = new Dao();
    }

    

    public async getEntry(){
            return await dao.getEntry()
    }

    public async createEntry(body: MovieDbModel){
        return await dao.createEntry(body)
    }

    public async deleteEntry(req: Request){
        return await dao.deleteEntry(req.params.id)
    }

    public async getById(req: Request){
        return await dao.getById(req.params.id)
    }

    public async updateEntry(req: Request){
        return await dao.updateEntry(req.params.id, req.body)

    }

}