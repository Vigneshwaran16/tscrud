import {MovieDbModel , modelSchema } from '../model/model'
import * as mongoose from 'mongoose'
import { Request } from 'express'

//All mongo queries are handled in dao.

const MovieDbModel = mongoose.model<MovieDbModel>('tsDB',modelSchema, 'movies')

export class Dao{

     public async getEntry(){
        return await MovieDbModel.find()
    }

    public async createEntry(body: MovieDbModel){
        let entry = new MovieDbModel(body)
        let savedEntry = entry.save()
        return savedEntry
    }

    public async deleteEntry(id: String){
        return await MovieDbModel.findByIdAndDelete(id) as MovieDbModel
        

    }

    public async getById(id: String){
        return await MovieDbModel.findById(id) as MovieDbModel
    }

    public async updateEntry(id: String, dataToUpdate: String){
        //let entry = new MovieDbModel(dataToUpdate)
        return await MovieDbModel.findOneAndUpdate({_id: id},dataToUpdate, {new:true}) as MovieDbModel
        //let docs = await MovieDbModel.findOneAndUpdate(id,{ $set : dataToUpdate}, {new:true})
        //console.log(docs)
        //return docs
    }


}