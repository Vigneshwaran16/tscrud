## tscrud

Typescript CRUD application following proper coding standards and proper oops concepts.


# Instructions for execution

- go to root directory and run `npm install`

- then, run `npm run dev` 

**All operations are appropriately commented**

